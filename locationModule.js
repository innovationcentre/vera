var cLocationModule = function() {
	var that = {};
	var standardSheet = {};
	var ataCodeSheet = {};

	that.testCases = function(){
		var testCaseSentenceInput = [];
		var testCaseSentenceExpect = [];
		//test case 0
		testCaseSentenceInput.push("I want to rebook my ticket.");
		testCaseSentenceExpect.push(null);
		//test case 1
		testCaseSentenceInput.push("I want to modify my flight on April 19th.");
		testCaseSentenceExpect.push(null);
		//test case 2
		testCaseSentenceInput.push("I would like to rebook my flight on next Thursday.");
		testCaseSentenceExpect.push(null);
		//test case 3
		testCaseSentenceInput.push("I want to change my flight on August 21st to 22nd.");
		testCaseSentenceExpect.push(null);
		//test case 4
		testCaseSentenceInput.push("I want to change my flight on April 24th to an earlier one on the same day.");
		testCaseSentenceExpect.push(null);
		//test case 5
		testCaseSentenceInput.push("I want to change my flight on May 3rd to the one leaving at 5pm.");
		testCaseSentenceExpect.push(null);
		//test case 6
		testCaseSentenceInput.push("I want to change my flight on May 3rd to the one leaving at 5.");
		testCaseSentenceExpect.push(null);
		//test case 7
		testCaseSentenceInput.push("I want to change my flight on May 3rd to the one leaving on 5th.");
		testCaseSentenceExpect.push(null);
		//test case 8
		testCaseSentenceInput.push("I've changed my mind and want to go earlier.");
		testCaseSentenceExpect.push(null);
		//test case 9
		testCaseSentenceInput.push("I've changed my mind and want to go later.");
		testCaseSentenceExpect.push(null);
		//test case 10
		testCaseSentenceInput.push("I want to take an earlier flight today.");
		testCaseSentenceExpect.push(null);
		//test case 11
		testCaseSentenceInput.push("I want to leave one day earlier for Frankfurt.");
		testCaseSentenceExpect.push(null);
		//test case 12
		testCaseSentenceInput.push("I want to leave two days later for London.");
		testCaseSentenceExpect.push(null);
		//test case 13
		testCaseSentenceInput.push("I want to modify my flight from Hong Kong to Frankfurt.");
		testCaseSentenceExpect.push("HongKong");
		//test case 14
		testCaseSentenceInput.push("I would like to rebook my return trip to Paris.");
		testCaseSentenceExpect.push(null);
		//test case 15
		testCaseSentenceInput.push("I want to modify my flight to Paris.");
		testCaseSentenceExpect.push(null);
		//test case 16
		testCaseSentenceInput.push("I would like to rebook my flight to France.");
		testCaseSentenceExpect.push(null);
		//test case 17
		testCaseSentenceInput.push("I'm not going to Paris anymore.");
		testCaseSentenceExpect.push(null);
		//test case 18
		testCaseSentenceInput.push("I'm planning on going to somewhere else.");
		testCaseSentenceExpect.push(null);
		//test case 19
		testCaseSentenceInput.push("I would like to change my flight to Paris to Frankfurt.");
		testCaseSentenceExpect.push(null);
		//test case 20
		testCaseSentenceInput.push("I would like to change my flight from Hong Kong to Paris to one to Frankfurt.");
		testCaseSentenceExpect.push("HongKong");
		//test case 21
		testCaseSentenceInput.push("I want to modify my flight to Paris in the coming week.");
		testCaseSentenceExpect.push(null);
		//test case 22
		testCaseSentenceInput.push("I want to modify my flight to Paris in the coming week to April 25th.");
		testCaseSentenceExpect.push(null);
		//test case 23
		testCaseSentenceInput.push("I want to modify my flight from Hong Kong to Paris on April 24th.");
		testCaseSentenceExpect.push("HongKong");
		//test case 24
		testCaseSentenceInput.push("I want to change my flight to 22.04. to 27.04.");
		testCaseSentenceExpect.push(null);
		//test case 25
		testCaseSentenceInput.push("Can I rebook my flight to leave on April 22 and return on April 27?");
		testCaseSentenceExpect.push(null);
		//test case 26
		testCaseSentenceInput.push("I need to change my flight to leave on Sunday and return next Wednesday.");
		testCaseSentenceExpect.push(null);
		//test case 27
		testCaseSentenceInput.push("Can I change my flight to leave on Sunday?");
		testCaseSentenceExpect.push(null);
		//test case 28
		testCaseSentenceInput.push("Can I change my flight from pudong");
		testCaseSentenceExpect.push("pudong");
		//test case 29
		testCaseSentenceInput.push("");
		testCaseSentenceExpect.push(null);


		var successFlag = true;
		for (var i = 0; i < testCaseSentenceInput.length; i++){
			// console.log("[Extract " +i+"]: " + testCaseSentenceInput[i]);
			successFlag = assert(that.extract(testCaseSentenceInput[i]), testCaseSentenceExpect[i]);
			if (!successFlag){
				console.log("FAILED: "+ i + "input: " + testCaseSentenceInput[i] + "expect: " + testCaseSentenceExpect[i] + "result: " + that.extract(testCaseSentenceInput[i]));
				break;
			}
		}

		//test the conversion
		if (successFlag){
			var testCaseLocationInput = [];
			var testCaseLocationExpect = [];

			//test case 0
			testCaseLocationInput.push("pudong");
			testCaseLocationExpect.push({location: "Shanghai", iataCode: ["PVG", "SHA"]});
			//test case 1
			testCaseLocationInput.push("sanfrancisco");
			testCaseLocationExpect.push({location: "San Francisco", iataCode: ["SFO"]});
			//test case 2
			testCaseLocationInput.push("bj");
			testCaseLocationExpect.push({location: "Beijing", iataCode: ["PEK"]});
			//test case 3
			testCaseLocationInput.push("pvg");
			testCaseLocationExpect.push({location: "Shanghai", iataCode: ["PVG", "SHA"]});
			//test case 4
			testCaseLocationInput.push("Beijing");
			testCaseLocationExpect.push({location: "Beijing", iataCode: ["PEK"]});
			//test case 5
			testCaseLocationInput.push("hOngkOng");
			testCaseLocationExpect.push({location: "Hong Kong", iataCode: ["HKG"]});
			//test case 6
			testCaseLocationInput.push("NonexistingPlace");
			testCaseLocationExpect.push({location: null, iataCode: null});

			for (var i = 0; i < testCaseLocationInput.length; i++){
				// console.log("[Convert " +i+"]: "+ testCaseLocationInput[i]);
				var result = that.convert(testCaseLocationInput[i])
				successFlag = assertObject(result, testCaseLocationExpect[i]);
				if (!successFlag){
					console.log("FAILED: "+ i + "input: " + testCaseLocationInput[i] + 
						" expect: " + 
						testCaseLocationExpect[i].location + " " + testCaseLocationExpect[i].iataCode + 
						" result: " + 
						result.location+ " "+ result.iataCode);
					break;
				}
			}
		}
		if (successFlag){
			console.log("[locationModule] Passed All Test Cases!");
		}
	}

	//assert
	function assert(result, expectation){
		if (result != expectation){
			return false;
		}
		return true;
	}

	//assertObject
	function assertObject(result, expectation){
		if (result.location != expectation.location){
			console.log("location unmatch: " + result.location + " " + expectation.location);
			return false;
		}
		if (result.iataCode != undefined){
			for (var i = 0; i < result.iataCode.length; i++){
				if (result.iataCode[i] != expectation.iataCode[i]){
					console.log("iataCode unmatch");
					return false;
				}
			}
		}
		return true;
	}


	that.extract = function(question) {
		const fs = require('fs');
		var data = fs.readFileSync('reference_data/cityCode.json').toString();
		standardSheet = JSON.parse(data);
		var spaceRemoved = removeSpace(question);
  		var location = keywordDetection(spaceRemoved);
  		// console.log("location extract: " + location);
		return location;
	}

	that.convert = function(locationRaw) {
		const fs = require('fs');
		var dataCity = fs.readFileSync('reference_data/cityCode.json').toString();
		standardSheet = JSON.parse(dataCity);
		var dataAirport = fs.readFileSync('reference_data/airport.json').toString();
		iataCodeSheet = JSON.parse(dataAirport);

		var standard = getStandard(locationRaw);
  		var iataCode = getIataCode(standard);
  		var result = {location: standard, iataCode: iataCode};
		
  		// console.log(result);

		return result;
	}

	
	//extract
	function standardSheetToString(standardSheet){
		var str = '';
		for (i = 0; i < standardSheet.length; i++){
			str += standardSheet[i].value + "|"
		};
			str = str.substring(0, str.length - 1);
		return str;
	}


	function keywordDetection(targetStr){
		strRegExp = standardSheetToString(standardSheet);
		var re=new RegExp(strRegExp, "gi");
		if(targetStr!= undefined){
			var matchStr = targetStr.match(re);
			return matchStr;
		}
	}

	function removeSpace(targetStr){
		if(targetStr!= undefined){
			var spaceRemoved = targetStr.replace(/\s+/g, '');
			return spaceRemoved;
		}
	}


	//convert
	function getStandard(value) {
		if (value != null){
			var result = standardSheet.filter(
				function(standardSheet) {
					return standardSheet.value == value.toLowerCase();
				}
			)

			if (result.length == 0){
				return getStandardName(value);//if it's captured by Alchemy -> change it to standard format

			}
			else return result[0].name;
		}
	}
	
	function searchCity(city) {
		return iataCodeSheet.filter(
			function(iataCodeSheet) {
				if (iataCodeSheet.city != null && city != undefined){
					return iataCodeSheet.city.toLowerCase() == city.toLowerCase();
				}
			}
		);
	}

	function searchCountry(country) {
		return iataCodeSheet.filter(
			function(iataCodeSheet) {
				if (iataCodeSheet.country != null && country != undefined){
					return iataCodeSheet.country.toLowerCase() == country.toLowerCase();
				}
			}
		);
	}

	function getIataCode(location){
		var code = [];
		if(location != null){
			// console.log("i'm here: "+location);
			var city = searchCity(location);
			
			for (i = 0; i < city.length; i++){
				code.push(city[i].iatacode);
			}
			if (code.length == 0){
				var country = searchCountry(location);
				for (i = 0; i < country.length; i++){
					code.push(country[i].iatacode);
				}
			}  
			return code;
		}
	}

	function getStandardName(location){
		var name = null;
		var city = searchCity(location);
		if (city && city.length > 0){
			name = city[0].city;
		}
	//alert(code);
		else {
			var country = searchCountry(location);
			if (country && country.length > 0){
				name = country[0].country;
			}
		}  
		return name;
	}


	var init = function() {

	}

	init();

	return that;
}

module.exports = new cLocationModule();