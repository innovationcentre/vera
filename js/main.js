/***********
	Project Vera Test Messenger
	Custom scripts
	(c) 2016 Cathay Pacific Airways Ltd.
***********/


// Global variables
var last_user;

var chatSound = document.createElement('audio');
    chatSound.setAttribute('src', '../media/space_rise1.wav');
    chatSound.setAttribute('type', 'audio/wav');
    chatSound.setAttribute('autoplay', 'autoplay');


// INIT Functions
function init() {
	//chatMessageEntryHandler(".chat-footer-message-entry");
	autosize($('textarea'));
	$('.chat-footer-message-entry').focus();
}


function sendMessage(message_input, chat_window) {
	var chat_message = $(message_input).val();

	//Mikita's code for submitting to server
	
	if (chat_message != "") {
		manageChatWindow(chat_message, chat_window, user="user");
	}

	//Clear message_input
	$(message_input).val('');
	$(message_input).attr('rows', '1');
}


function manageChatWindow(chat_message, chat_window, user) {

	//check if a chat block exists, if not create one and add message
	if(!$(".chat-block")[0] || user != last_user)  {

		$(chat_window).append(
    		'<div class="row chat-block ' + user + '">' +
			'<div class="col-lg-12">' +
			'<ul class="chat-list"></ul>' +
			'</div>' +
			'</div> <!-- /.chat-block /.row -->'
		).clone();

		makeChatBubble(chat_message);
		
	} else if (user = last_user) {

		makeChatBubble(chat_message);
	} 

	last_user = user;
}

function makeChatBubble (chat_message) {

	//change to CSS3 animation
	$(".chat-list").last().append('<li class="chat-bubble" style="display: none;">' + chat_message + '</li>').find('.chat-bubble').fadeIn('slow');
	$('.test_audio')[0].play();
}
