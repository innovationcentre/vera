var cSampleModule = function() {
    var moment = require('moment');

	var that = {};

	that.AlchemyExtractLocationRequestMethodRequestMethod = function(data, successCallback, failureCallback) {
    	console.log("in AlchemyExtractLocationRequestMethodRequestMethod");
    	var request1 = require('request');
        request1({
            url: 'http://access.alchemyapi.com/calls/text/TextGetRankedNamedEntities', //URL to hit
            qs: {
                apikey: '6232eaddfe09f1362414b35172a5df7ebb454244', 
                outputMode: 'json',
                showSourceText:'1',
                anchorDate: moment().format("YYYY-MM-DD HH:mm:ss"),
                text:data
            }, //Query string data
            method: 'POST', //Specify the method
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = [];

                var parsing = JSON.parse(body);

                // console.log(parsing);

                if (parsing['entities']) {
                    for (var i=0; i < parsing['entities'].length; i++) {
                        var entities = parsing.entities;
                        var entity = entities[i];
                        var type = entity.type.toLowerCase();
                        if (type == "city" || type == "country") {
                            result.push(entity.text);
                        }
                    }
                }

                if (successCallback) {
                    successCallback(result);
                }
            }else{
                console.log(error);
                if (failureCallback) {
                    failureCallback(error);
                }            
            }
        });
    }

    that.AlchemyExtractDateRequestMethodRequestMethod = function(data, successCallback, failureCallback) {
        console.log("in AlchemyExtractDateRequestMethodRequestMethod");
        var request1 = require('request');
        request1({
            url: 'http://gateway-a.watsonplatform.net/calls/text/TextExtractDates', //URL to hit
            qs: {
                apikey: '6232eaddfe09f1362414b35172a5df7ebb454244', 
                outputMode: 'json',
                showSourceText:'1',
                anchorDate: moment().format("YYYY-MM-DD HH:mm:ss"),
                text:data
            }, //Query string data
            method: 'POST', //Specify the method
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = [];

                var parsing = JSON.parse(body);

                // console.log(parsing);

                if (parsing['dates']) {
                    for (var i=0; i < parsing['dates'].length; i++) {
                        var entity = parsing.dates[i];
                        if (entity.date) {
                            result.push(entity.date);
                        }
                    }
                }

                if (successCallback) {
                    successCallback(result);
                }

            }else{
                console.log(error);
                if (failureCallback) {
                    failureCallback(error);
                }            
            }

        });
    }

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cSampleModule();
