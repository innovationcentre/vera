var cTimeExtraction = function (){
   
   var that = {};

  /*0 means later,1 means earlier/
  /*shared with time extraction engine*/
  var time_status_array = [0,1];
  var date_keyword_array = ["next_week","same_day"];
  var UserException = function (err_msg) {
     this.error = err_msg;
  };

  that.getTimeStatus = function(){

    return time_status_array;

   }

  that.getDateKeyWord = function(){

    return date_keyword_array;

  }

  //TIME EXTRACTION
/*extract time status keyword for later or earlier */
that.time_keyword_extraction = function (targetStr){
var late_re=new RegExp("later|(cannot make)|late", "gi");
var early_re=new RegExp("earlier", "gi");

  if(targetStr.match(late_re)){
      return time_status_array[0];
  }else if(targetStr.match(early_re)){
      return time_status_array[1];
  }
  return -1;
}

  that.time_extraction = function (timeStr){
        var re = /([0-9])(\:[0-5][0-9])?\s*[ap]m/gi;
        var AMPM = timeStr.match(re);
        return AMPM;
  }

  //TIME EXTRACTION END
  //DATE EXTRACTION START
  /*This method used to extract some*/
  that.date_keyword_extraction = function(targetStr){
  var nextW_re = new RegExp("(next|coming) week", "gi");
  var same_re = new RegExp("same day", "gi");

    if(targetStr.match(nextW_re)){
        return date_keyword_array[0];
    }else if(targetStr.match(same_re)){
        return date_keyword_array[1];
    }
    return -1;

  }

  that.extractTodayTomo = function (dateDurStr){
     var re = new RegExp("today|tomorrow", "gi");
     if (!dateDurStr.match(re)) return -1;
     return dateDurStr.match(re);
  } 

  that.number_extraction = function (numberStr){
        var re = /\d+/g;
        return numberStr.match(re);
    }

  that.month_date_extraction = function (mon_date_Str){

    var basic_date = new RegExp("((jan|feb)(uary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sept(ember)?|Oct(ober)?|(Nov|Dec)(ember)?) (0?[1-9]|[1-3][0-9])(?:st|nd|rd|th)","gi");
    var full_date_match_Ary = mon_date_Str.match(basic_date);
    var re_month = new RegExp("(jan|feb)(uary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sept(ember)?|Oct(ober)?|(Nov|Dec)(ember)?","gi");
    var months_match_Ary = mon_date_Str.match(re_month);
    var re_day = new RegExp("(0?[1-9]|[1-3][0-9])(?:st|nd|rd|th)","gi");
    var days_match_Ary = mon_date_Str.match(re_day);
    if (!days_match_Ary)
      return [];

    var resultDateAry = [];
      for (var i = 0; i < days_match_Ary.length; i++) {
          var temp_day = days_match_Ary[i];
          var is_curday_has_month = false;
          var curDate;
          /*add month for curDate*/
          if(months_match_Ary.length == days_match_Ary.length){
              curDate = months_match_Ary[i]+" "+ temp_day.replace(/st|nd|rd|th/gi, "");
           }else{
              curDate = months_match_Ary[0]+" "+ temp_day.replace(/st|nd|rd|th/gi, "");
           }
          resultDateAry.push(curDate);      
      }
      
       return resultDateAry;
    }

  that.extractDurationStatus = function (dateDurStr){
  var re = new RegExp( "(one|t(wo|hree|en|welve|hirteen|wenty|hirty)|f(our|ive|ourteen|iftenn|orty|ifty)|s(ixteen|ixty|eventy|ix|even|eventeen|teen)|eigh(ty|t|teen)|nin(ety|e|eteen)|zero|hundred|thousand|million) day(s)? (later|earlier)", "gi" );
  
  var dateDurMatch = dateDurStr.match(re);
  if(!dateDurMatch) return -1;
  return dateDurMatch;
}

that.extractCommonDate = function (dateStr){
   
    var re = new RegExp("([0-9]{2}[/|\.|-][0-9]{2}[/|\.|-][0-9]{4})|([0-9]{4}[/|\.|-][0-9]{2}[/|\.|-][0-9]{2})|([0-9]{2}[/|\.|-][0-9]{2})", "gi");
    if(dateStr.match(re)){
    return dateStr.match(re);
    }

   return -1;
}


   return that;
}
module.exports = new cTimeExtraction();


