var timeExtractionEngine = require('./timeExtractionEngine.js');
var timeConvertEngine = require('./timeConvertEngine.js');

var cDateTimeTestModule = function() {
    var that = {};

    that.Test = function(){
		testTimeExtraction();
        console.log("[TimeExtraction] Passed All Test Cases");
		testDateExtraction();
        console.log("[DateExtraction] Passed All Test Cases");
		testTimeConvert();
        console.log("[TimeConvert] Passed All Test Cases");
		testDateConvert();
        console.log("[DateConvert] Passed All Test Cases");
    }

    var testTimeExtraction = function(){
        var testCaseAry = getTestQuestionAry();
         //Test extract time with am and pm from string
         var testTimeInputStr = testCaseAry[5];
         var time_extraction_re = timeExtractionEngine.time_extraction(testTimeInputStr);
         assertError( time_extraction_re, '5pm', "5pm extract fail" );
         testTimeInputStr += " and 5:30AM";
         time_extraction_re = timeExtractionEngine.time_extraction(testTimeInputStr);
         assertError( time_extraction_re[1], '5:30AM', "multiple AM/PM extract fail" );
         testTimeInputStr = testCaseAry[6];
         time_extraction_re = timeExtractionEngine.time_extraction(testTimeInputStr);
         assertError( time_extraction_re, null, "no am or pm extrac fail" );

         //Test extract time status keyword for for later or earlier
         testTimeInputStr = testCaseAry[8];
         time_extraction_re = timeExtractionEngine.time_keyword_extraction(testTimeInputStr);
         assertError( time_extraction_re, 1, "earlier detect fail" );
         testTimeInputStr = testCaseAry[9];
         time_extraction_re = timeExtractionEngine.time_keyword_extraction(testTimeInputStr);
         assertError( time_extraction_re, 0, "later detect fail" );
         testTimeInputStr = testCaseAry[0];
         time_extraction_re = timeExtractionEngine.time_keyword_extraction(testTimeInputStr);
         assertError( time_extraction_re, -1, "no time staus keyword handle fail" );
    
    }

    var testDateExtraction = function(){

         var testCaseAry = getTestQuestionAry();
         var testTimeInputStr = testCaseAry[21];
         var date_extraction_re = timeExtractionEngine.date_keyword_extraction(testTimeInputStr);
         assertError( date_extraction_re, 'next_week', "coming week extract fail" );
         testTimeInputStr = testCaseAry[4];
         date_extraction_re = timeExtractionEngine.date_keyword_extraction(testTimeInputStr);
         assertError( date_extraction_re, 'same_day', "coming week extract fail" );
         testTimeInputStr = testCaseAry[0];
         date_extraction_re = timeExtractionEngine.date_keyword_extraction(testTimeInputStr);
         assertError( date_extraction_re, -1, "no date keyword handle fail" );


         testTimeInputStr = testCaseAry[10];
         date_extraction_re = timeExtractionEngine.extractTodayTomo(testTimeInputStr);
         assertError( date_extraction_re, 'today', "today extract fail" );
         testTimeInputStr += " or TOMORROW";
         date_extraction_re = timeExtractionEngine.extractTodayTomo(testTimeInputStr);
         assertError( date_extraction_re[1], 'TOMORROW', "multiple today/tomo extract fail" );
         testTimeInputStr = testCaseAry[0];
         date_extraction_re = timeExtractionEngine.extractTodayTomo(testTimeInputStr);
         assertError( date_extraction_re, -1, "no today handle fail" );
      
         
         testTimeInputStr = testCaseAry[6];
         date_extraction_re = timeExtractionEngine.number_extraction(testTimeInputStr);
         assertError( date_extraction_re[1], 5, "number extract fail" );
         testTimeInputStr = testCaseAry[0];
         date_extraction_re = timeExtractionEngine.number_extraction(testTimeInputStr);
         assertError( date_extraction_re, null, "no number handle fail" );


         testTimeInputStr = testCaseAry[3];
         date_extraction_re = timeExtractionEngine.month_date_extraction(testTimeInputStr);
         assertError( date_extraction_re[1], 'August 22', "month date extract fail" );
        
         testTimeInputStr = testCaseAry[4] +"and May 23nd";
         date_extraction_re = timeExtractionEngine.month_date_extraction(testTimeInputStr);
         assertError( date_extraction_re, 'April 24,May 23', "month date extract fail" );
         testTimeInputStr = testCaseAry[25];
         date_extraction_re = timeExtractionEngine.month_date_extraction(testTimeInputStr);
         assertError( date_extraction_re.length, 0, "month date handle fail" );

         //extractDurationStatus
         testTimeInputStr = testCaseAry[11];
         date_extraction_re = timeExtractionEngine.extractDurationStatus(testTimeInputStr);
         assertError( date_extraction_re, 'one day earlier', "duration day earlier/later extract fail" );
         
         testTimeInputStr += testCaseAry[12];

         date_extraction_re = timeExtractionEngine.extractDurationStatus(testTimeInputStr);
         assertError( date_extraction_re[1], 'two days later', "multiple duration day earlier/later extract fail" );
         testTimeInputStr = testCaseAry[0];
         date_extraction_re = timeExtractionEngine.extractDurationStatus(testTimeInputStr);
         assertError( date_extraction_re, -1, "no duration day earlier/laterhandle fail" );

         //extractCommonDate
         testTimeInputStr = testCaseAry[24];
         date_extraction_re = timeExtractionEngine.extractCommonDate(testTimeInputStr);
         assertError( date_extraction_re, '22.04,27.04', "dd.mm extract fail" );
         testTimeInputStr = testCaseAry[24]+" and 04.27 and 2016.04.27 and 2016/04/27 and 2016-04-27";
         date_extraction_re = timeExtractionEngine.extractCommonDate(testTimeInputStr);
         assertError(date_extraction_re, '22.04,27.04,04.27,2016.04.27,2016/04/27,2016-04-27', "multiple dates extract fail" );         testTimeInputStr = testCaseAry[0];
         date_extraction_re = timeExtractionEngine.extractCommonDate(testTimeInputStr);
         assertError( date_extraction_re, -1, "no dates handle fail" );

    }

    var testTimeConvert = function(){
        var testTimeInputStr = "";

        //am_pm_to_hours
        var am_pm_to_hours = "";
        testTimeInputStr = "5pm"
        am_pm_to_hours = timeConvertEngine.am_pm_to_hours(testTimeInputStr);
        assertError( am_pm_to_hours, '1700', "am_pm_to_hours fail" );
        testTimeInputStr = "5:30AM"
        am_pm_to_hours = timeConvertEngine.am_pm_to_hours(testTimeInputStr);
        assertError( am_pm_to_hours, '0530', "am_pm_to_hours fail" );

        testTimeInputStr = "5:30"
        am_pm_to_hours = timeConvertEngine.am_pm_to_hours(testTimeInputStr);
        assertError( am_pm_to_hours, -1, "no am/pm handle fail" );

    }

    var testDateConvert = function(){
        //alchemyDate_to_standardDate
        var standardDateConvertRe = "";
        var dateConvertAry = getDateConvertAry();

        //getCommingWeek 
        standardDateConvertRe = timeConvertEngine.getCommingWeek();
        assertError(standardDateConvertRe, '160508,160514', "getCommingWeek fail");

        //getTodayTomorrowStandardDate
        for(var i = 0;i<dateConvertAry.length;i++){
            standardDateConvertRe = timeConvertEngine.getTodayTomorrowStandardDate(dateConvertAry[i]);

            if (i == 3) {
                assertError( standardDateConvertRe, '160505', "getTodayTomorrowStandardDate fail");
            }else{
                 assertError( standardDateConvertRe, -1, "getTodayTomorrowStandardDate fail");
            }
        }
        //alchemyDate_to_standardDate
        for(var i = 0;i<dateConvertAry.length;i++){
            standardDateConvertRe = timeConvertEngine.alchemyDate_to_standardDate(dateConvertAry[i]);
            if (i == 0) {
                assertError( standardDateConvertRe, '160419', "alchemyDate_to_standardDate fail");
            }else{
                 assertError( standardDateConvertRe, -1, "alchemyDate_to_standardDate fail");
            }
        }
        //getDateFromDuration
        for(var i = 0;i<dateConvertAry.length;i++){
            var isHasTimeStatus = timeExtractionEngine.time_keyword_extraction(dateConvertAry[i]);
            standardDateConvertRe = timeConvertEngine.getDateFromDuration(dateConvertAry[i],isHasTimeStatus,new Date());
            if (i == 1) {
                assertError( standardDateConvertRe, '160503', "getDateFromDuration fail");
            }else if (i == 2){
                assertError( standardDateConvertRe, '160506', "getDateFromDuration fail");
            }else{
                assertError( standardDateConvertRe, -1, "getDateFromDuration fail");
            }
        }

        //cardinalDate_to_standardDate
        for(var i = 0;i<dateConvertAry.length;i++){
            standardDateConvertRe = timeConvertEngine.cardinalDate_to_standardDate(dateConvertAry[i]);
            if (i == 4) {
                 assertError( standardDateConvertRe, '160424', "cardinalDate_to_standardDate 4 fail");
            }else if (i == 5){
                 assertError( standardDateConvertRe, '160422', "cardinalDate_to_standardDate 5 fail");
            }else if (i == 6){
                 assertError( standardDateConvertRe, '160427', "cardinalDate_to_standardDate 6 fail");
            }else if (i == 7){
                 assertError( standardDateConvertRe, '160427', "cardinalDate_to_standardDate 7 fail");
            }else if (i == 8){
                 assertError( standardDateConvertRe, '160427', "cardinalDate_to_standardDate 8 fail");
            }else if (i == 9){
                 assertError( standardDateConvertRe, '160427', "cardinalDate_to_standardDate 9 fail");
            }else{
                 assertError( standardDateConvertRe, -1, "cardinalDate_to_standardDate fail");
            }
        }  
    }

    var assertError = function(result,expect,error){
        if(result != expect){
            console.log(error+" result is:"+result+" expect is:"+expect);
        }
    }

    var getDateConvertAry = function(){
        var testCaseAry = [];
        //0
        testCaseAry.push("20160419T000000");
        //1
        testCaseAry.push("one day earlier");
        //2
        testCaseAry.push("two days later");
        //3
        testCaseAry.push("TOMORROW");
        //4
        testCaseAry.push("April 24");
        //5
        testCaseAry.push("22.04");
        //6
        testCaseAry.push("04.27");
        //7
        testCaseAry.push("2016.04.27");
        //8
        testCaseAry.push("2016/04/27");
        //9
        testCaseAry.push("2016-04-27");
        //10
        testCaseAry.push("2016-27-04");

        return testCaseAry;
    }


    var getTestQuestionAry = function(){

    var testCaseAry = [];
     //test case 1
     testCaseAry.push("I want to rebook my ticket.");
     //test case 2
     testCaseAry.push("I want to modify my flight on April 19th.");
     //test case 3
      testCaseAry.push("I would like to rebook my flight on next Thursday.");
      //test case 4
     testCaseAry.push("I want to change my flight on August 21st to 22nd.");
     //test case 5
     testCaseAry.push("I want to change my flight on April 24th to an earlier one on the same day ");
    //test case 6
     testCaseAry.push("I want to change my flight on May 3rd to the one leaving at 5pm.");
     //test case 7
     testCaseAry.push("I want to change my flight on May 3rd to the one leaving at 5.");
     //test case 8
     testCaseAry.push("I want to change my flight on May 3rd to the one leaving on 5th.");
     //test case 9
     testCaseAry.push("I've changed my mind and want to go earlier.");
     //test case 10
     testCaseAry.push("I've changed my mind and want to go later.");
     //test case 11
     testCaseAry.push("I want to take an earlier flight today.");
     //test case 12
     testCaseAry.push("I want to leave one day earlier for Frankfurt.");
     //test case 13
     testCaseAry.push("I want to leave two days later for London.");
     //test case 14
     testCaseAry.push("I want to modify my flight from Hong Kong to Frankfurt.");
     //test case 15
     testCaseAry.push("I would like to rebook my return trip to Paris.");
     //test case 16
     testCaseAry.push("I want to modify my flight to Paris.");
     //test case 17
     testCaseAry.push("I would like to rebook my flight to France.");
     //test case 18
     testCaseAry.push("I'm not going to Paris anymore.");
     //test case 19
     testCaseAry.push("I'm planning on going to somewhere else.");
     //test case 20
     testCaseAry.push("I would like to change my flight to Paris to Frankfurt.");
     //test case 21
     testCaseAry.push("I would like to change my flight from Hong Kong to Paris to one to Frankfurt.");
      //test case 22
     testCaseAry.push("I want to modify my flight to Paris in the coming week.");
     //test case 23
     testCaseAry.push("I want to modify my flight to Paris in the coming week to April 25th.");
     //test case 24
     testCaseAry.push("I want to modify my flight from Hong Kong to Paris on April 24th.");
     //test case 25
     testCaseAry.push("I want to change my flight to 22.04. to 27.04");
     //test case 26
     testCaseAry.push("Can I rebook my flight to leave on April 22 and return on April 27?");
     //test case 27
     testCaseAry.push("I need to change my flight to leave on Sunday and return next Wednesday.");
     //test case 28
     testCaseAry.push("Can I change my flight to leave on Sunday?");

     return testCaseAry;

    }

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cDateTimeTestModule();