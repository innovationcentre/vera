var cSampleModule = function() {
	var that = {};

	that.naturalLanguageProcessingRequestMethod = function(data, successCallback, failureCallback) {
    	console.log("in naturalLanguageProcessingRequestMethod");
		var result = "";
    	var request1 = require('request');
    	request1(
            {
                url: 'https://9bb48164-6a5f-40d5-85e4-845fa115edab:ScStTfOA5Qt3@gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers/3a84dfx64-nlc-2125/classify', //URL to hit
                qs: {
                    outputMode: 'json',
                    showSourceText:'1',
                    anchorDate:'2016-04-22 11:07:00',
                    text:data
                }, //Query string data
            }, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                	var parsing = JSON.parse(body);
                	console.log(parsing);
                	for (var i=0; i < parsing['classes'].length; i++){
                		if(parsing['classes'][i].class_name  === 'Rebooking' && parseFloat(parsing['classes'][i].confidence) > "0.8"){
                			result = "rebooking";
                			break;
                		}
                	}

                    if (successCallback) {
                        successCallback(result);
                    }
                } else{
                    console.log(error);
                    if (failureCallback) {
                        failureCallback(error);
                    }
                }
            }
        );
	}

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cSampleModule();
