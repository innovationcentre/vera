var cSampleModule = function() {
	var that = {};

	that.myFunc = function(a) {
		return "returning " + a;
	}

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cSampleModule();
