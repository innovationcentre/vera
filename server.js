var http = require("http");
var qs = require("querystring");
var StringBuilder = require("stringbuilder");
var request = require('request');
var fs = require('fs');
var url = require('url');
var path = require('path');

var sampleModule = require('./sampleModule.js');
var naturalLanguageClassifierModule = require('./naturalLanguageClassifierModule.js');
var alchemyModule = require('./alchemyModule.js');

var locationModule = require('./locationModule.js');
var flightNumModule = require('./flightNumModule.js');
var dateModule = require('./dateModule.js');
var timeModule = require('./timeModule.js');
var cDateTimeTestModule = require('./DateTimeModuleTest.js');
var logger = require('./logHelper.js');

var port = 9000;
var result = "";

function askVera(question, callback) {
    //logger.log("test.txt","USER:" + question);
    logger.textLogChat();

	var answer = {};

    var nlcHandler = function(category) {
    	console.log("class=" + category);

    	if (category == "rebooking") {
    		answer["category"] = "rebooking";

    		alchemyModule.AlchemyExtractLocationRequestMethodRequestMethod(question, alchemyLocationHandler);
		}
    	else {
		    if (callback) {
		    	callback(JSON.stringify("Sorry, I can't handle questions other than rebooking now!"));
		    }    		
    	}
    }

    var alchemyLocationHandler = function(alchemyResult) {
    	console.log("alchemyLocationHandler=" + JSON.stringify(alchemyResult));

    	// extract the locations by our own module, too
    	var locations = locationModule.extract(question);    
    	// var locations = [];

    	var merged = alchemyResult.concat(locations);
    	// Todo: remove duplicates

    	var locationsFinal = [];
    	merged.forEach(function(location) {
	        var locationConverted = locationModule.convert(location);
	        locationsFinal.push(locationConverted);
    		console.log(location + " -> " + JSON.stringify(locationConverted));
    	});

    	answer["locations"] = locationsFinal;

    	alchemyModule.AlchemyExtractDateRequestMethodRequestMethod(question, alchemyDateHandler);
    }

    var alchemyDateHandler = function(alchemyResult) {
    	console.log("alchemyDateHandler=" + JSON.stringify(alchemyResult));

    	// extract the dates by our own module, too
    	var dates = dateModule.extractDate(question);    

    	var merged = alchemyResult.concat(dates);
    	// Todo: remove duplicates

    	var datesFinal = [];
    	merged.forEach(function(date) {
	        var dateConverted = dateModule.convertDate(date);
	        datesFinal.push(dateConverted);
    		console.log(date + " -> " + JSON.stringify(dateConverted));
    	});

    	answer["dates"] = datesFinal;

    	processTime();
    }

    var processTime = function() {
    	// extract the times by our own module
    	var times = timeModule.extractTime(question);    

    	var timesFinal = [];
    	times.forEach(function(time) {
	        var timeConverted = timeModule.convertTime(time);
	        timesFinal.push(timeConverted);
    		console.log(time + " -> " + JSON.stringify(timeConverted));
    	});

    	answer["times"] = timesFinal;

	    if (callback) {
	    	callback(JSON.stringify(answer));
	    } 

	    flightNumHandler();   		
    }

    var flightNumHandler = function(){
    	var flightNum = flightNumModule.extract(question);
    	console.log(flightNum);
    }

    // Watson Natural Language Classifier
    naturalLanguageClassifierModule.naturalLanguageProcessingRequestMethod(question, nlcHandler, function() {	    
		    if (callback) {
		    	callback(JSON.stringify("Some issues on network, please try again..."));
		    }    		

    });

    return;
}

function showError(req, resp){
	resp.writeHead(400,{'Content-type':"text/plain"});
	resp.write("Page Not Found!!!");
	resp.end();
}

function getAlchemyForm(req,resp,formData){
	resp.writeHead(200,{'Content-type':"text/html"});
	getAlchemyHtml(req,resp,formData);
	
}

http.createServer(function(req,resp){

    cDateTimeTestModule.Test();

	console.log("Enters in createServer");

	locationModule.testCases();
	flightNumModule.testCases();
	// logger.textLogChat();

    var query = url.parse(req.url, true).query;
    var pathname = url.parse(req.url, true).pathname;
    var extname = path.extname(pathname);
	switch(req.method){
		case 'GET':
		result = "";
		if (pathname === "/"){
	        resp.writeHead(200, {"Context-Type" : "text\plain"});
	        fs.createReadStream ('./index.html').pipe(resp);
		}else if(pathname === "/alchemy"){
			getAlchemyForm(req,resp);
        }
        else if(pathname === "/askVera") {
            if (query != null && query.q) {
                askVera(query.q, function(result) {
                	console.log("vera answer: " + result);
	                resp.writeHead(200, {"Content-Type": "application/json"});
	                resp.end(JSON.stringify(result));
	            });
            }
        }else if(pathname === "/test"){
	        resp.writeHead(200, {"Context-Type" : "text\plain"});
    	    fs.createReadStream ('./index-test.html').pipe(resp);
        }else if(extname === ".js"){
            
        	var contentType = "text/javascript";
            fs.readFile(pathname,function (error,content){
                if (error) {
                	 console.log("Enters err code:"+error.code);
				        if(error.code == 'ENOENT'){

				            fs.readFile('./404.html', function(error, content) {
				                resp.writeHead(200, { 'Content-Type': contentType });
				                resp.end(content, 'utf-8');
				            });
				        }
				        else {
				        	 console.log("Enters in err: 500");
				            resp.writeHead(500);
				            resp.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
				            resp.end(); 
				        }
                }else{
                	  resp.writeHead(200, { 'Content-Type': contentType });
                      resp.end(content, 'utf-8');
                }
            });

        }
		else{
			showError(req,resp);
		}
		break;

		case 'POST':
		if(pathname === '/alchemy'){
			var reqBody = '';
			req.on('data',function(data){
				reqBody += data;
			});
			req.on('end',function(data){
				var formData = qs.parse(reqBody);
				if(formData){
					naturalLanguageProcessingRequestMethod(req,resp,formData);
				}

			});
		}else{
			showError(req,resp);
		}
		break;
		default:
			showError(req,resp);
		break;
	}
	
}).listen(port);
