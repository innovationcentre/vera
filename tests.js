
QUnit.test( "Hello test", function( assert ) {
 assert.ok( true, "this test is fine" );
});


QUnit.test( "test date extraction", function( assert ) {
 
 var timeExtractionEngine = cTimeExtraction();
 var testCaseAry = getTestCaseAry();

 var time_extraction = timeExtractionEngine.time_extraction(testCaseAry[5]);
 assert.equal( time_extraction, '5pm', "5pm extract suceess" );

});


function getTestCaseAry(){

var testCaseAry = [];
 //test case 1
 testCaseAry.push("I want to rebook my ticket.");
 //test case 2
 testCaseAry.push("I want to modify my flight on April 19th.");
 //test case 3
  testCaseAry.push("I would like to rebook my flight on next Thursday.");
  //test case 4
 testCaseAry.push("I want to change my flight on August 21st to 22nd.");
 //test case 5
 testCaseAry.push("I want to change my flight on April 24th to an earlier one on the same day.");
//test case 6
 testCaseAry.push("I want to change my flight on May 3rd to the one leaving at 5pm.");
 //test case 7
 testCaseAry.push("I want to change my flight on May 3rd to the one leaving at 5.");
 //test case 8
 testCaseAry.push("I want to change my flight on May 3rd to the one leaving on 5th.");
 //test case 9
 testCaseAry.push("I've changed my mind and want to go earlier.");
 //test case 10
 testCaseAry.push("I've changed my mind and want to go later.");
 //test case 11
 testCaseAry.push("I want to take an earlier flight today.");
 //test case 12
 testCaseAry.push("I want to leave one day earlier for Frankfurt.");
 //test case 13
 testCaseAry.push("I want to leave two days later for London.");
 //test case 14
 testCaseAry.push("I want to modify my flight from Hong Kong to Frankfurt.");
 //test case 15
 testCaseAry.push("I would like to rebook my return trip to Paris.");
 //test case 16
 testCaseAry.push("I want to modify my flight to Paris.");
 //test case 17
 testCaseAry.push("I would like to rebook my flight to France.");
 //test case 18
 testCaseAry.push("I'm not going to Paris anymore.");
 //test case 19
 testCaseAry.push("I'm planning on going to somewhere else.");
 //test case 20
 testCaseAry.push("I would like to change my flight to Paris to Frankfurt.");
 //test case 21
 testCaseAry.push("I would like to change my flight from Hong Kong to Paris to one to Frankfurt.");
  //test case 22
 testCaseAry.push("I want to modify my flight to Paris in the coming week.");
 //test case 23
 testCaseAry.push("I want to modify my flight to Paris in the coming week to April 25th.");
 //test case 24
 testCaseAry.push("I want to modify my flight from Hong Kong to Paris on April 24th.");
 //test case 25
 testCaseAry.push("I want to change my flight to 22.04. to 27.04.");
 //test case 26
 testCaseAry.push("Can I rebook my flight to leave on April 22 and return on April 27?");
 //test case 27
 testCaseAry.push("I need to change my flight to leave on Sunday and return next Wednesday.");
 //test case 28
 testCaseAry.push("Can I change my flight to leave on Sunday?");

 return testCaseAry;

}