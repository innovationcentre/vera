var cFlightNumModule = function() {
	var that = {};

	that.testCases = function(){
		var testCaseSentenceInput = [];
		var testCaseSentenceExpect = [];
		//test case 0
		testCaseSentenceInput.push("I want to rebook my ticket CX2731");
		testCaseSentenceExpect.push("CX2731");
		//test case 1
		testCaseSentenceInput.push("I want to modify my flight on April 19th.");
		testCaseSentenceExpect.push(null);
		//test case 2
		testCaseSentenceInput.push("I would like to rebook my flight KA 301 on next Thursday.");
		testCaseSentenceExpect.push("KA301");
		//test case 3
		testCaseSentenceInput.push("I want to change my flight kA132 on August 21st to 22nd.");
		testCaseSentenceExpect.push("KA132");
		//test case 4
		testCaseSentenceInput.push("I want to change my flight on April 24th to an earlier one on the same day with Katherine.");
		testCaseSentenceExpect.push(null);
		//test case 5
		testCaseSentenceInput.push("I want to change my flight CX4812638172.");
		testCaseSentenceExpect.push(null);
		//test case 6
		testCaseSentenceInput.push("I want to change my flight CX42.");
		testCaseSentenceExpect.push(null);
		//test case 7
		testCaseSentenceInput.push("Rebook cx231");
		testCaseSentenceExpect.push("CX231");
		


		var successFlag = true;
		for (var i = 0; i < testCaseSentenceInput.length; i++){
			// console.log("[Extract " +i+"]: " + testCaseSentenceInput[i]);
			successFlag = assert(that.extract(testCaseSentenceInput[i]), testCaseSentenceExpect[i]);
			if (!successFlag){
				console.log("FAILED: "+ i + "input: " + testCaseSentenceInput[i] + "expect: " + testCaseSentenceExpect[i] + "result: " + that.extract(testCaseSentenceInput[i]));
				break;
			}
		}

		if (successFlag){
			console.log("[flightNumModule] Passed All Test Cases!");
		}
	}

	//assert
	function assert(result, expectation){
		if (result != expectation){
			return false;
		}
		return true;
	}


	that.extract = function(question) {
		var spaceRemoved = removeSpace(question);
  		var flightNumStart = keywordDetection(spaceRemoved);
  		if (flightNumStart!= null){
  			var flightNum = getFlightNum(spaceRemoved, flightNumStart).toUpperCase();
  			if (flightNum.length > 4 && flightNum.length< 7){
				return flightNum;
			}
			else return null;
		}
		return null;
	}

	function keywordDetection(targetStr){
		strRegExp = "CX|KA";
		var re=new RegExp(strRegExp, "gi");

		var match = re.exec(targetStr);
		if(targetStr!= undefined){
			if (match) {
			    return match.index;
			}
		}
		return null;
	}

	function removeSpace(targetStr){
		if(targetStr!= undefined){
			var spaceRemoved = targetStr.replace(/\s+/g, '');
			return spaceRemoved;
		}
	}

	function getFlightNum(spaceRemoved, flightNumStart){
		var flightNum = spaceRemoved[flightNumStart] + spaceRemoved[flightNumStart + 1]; 
		for (var i = 2; i < spaceRemoved.length - flightNumStart; i++){
	  		if (Number.isInteger(parseInt(spaceRemoved[flightNumStart + i]))){
	  			// console.log("yes: " + spaceRemoved[flightNumStart + i]);
	  			flightNum += spaceRemoved[flightNumStart + i];
	  		}else {
	  			break;
	  		}
  		}
  		return flightNum;
	}

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cFlightNumModule();