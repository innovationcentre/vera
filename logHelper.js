var logger = function(){
  
  var that = {};
  var fs = require('fs');
  var logRootPath = "./logs/";
  that.log = function(path,text){
    fs.appendFileSync(path, text);
  }
  
  that.chatLog = function(fileName,isQuestion,text){
   
    //extract userToken later for dictionary
    var full_path = logRootPath+"chat_logs/"+fileName;
    checkForFile(full_path);
    text = text.replace(',',';');
    if(isQuestion){
      text = "\r\n["+getTimeStamp()+"]USER:"+text+",";
    }else{
      text = "["+getTimeStamp()+"]VERA:"+text+";";
    }
    that.log(full_path,text);
  }

  that.basicLog = function(fileName,text){
     var full_path = logRootPath+fileName;
     text = "["+getTimeStamp()+"]"+text;
     that.log(full_path,text+"\n");

  }

  var checkForFile = function(path){
    if(!fs.existsSync(path)) {
       that.log(path,"USER,VERA");
    }
  }

  var getTimeStamp = function(){
      
     //return Date();
     return Date.now();
  }


  that.textLogChat = function(){
    var qTestAry = ["This is question 1,please add me","This is question 2,please add me","This is question 3","This is question 4","This is question 5"];
    var aTestAry = ["This is answer 1,please add me","This is answer 2,please add me","This is answer 3","This is answer 4","This is answer 5"];
    var testUserToken = "qiao_token7";
    testUserToken += ".txt";
    for(var i = 0; i < qTestAry.length ;i++){
      that.chatLog(testUserToken,true,qTestAry[i]);
      that.chatLog(testUserToken,false,aTestAry[i]);

      that.basicLog(testUserToken,"USER:"+qTestAry[i]);
      that.basicLog(testUserToken,"VERA:"+aTestAry[i]);
      readFile(testUserToken);
    }
       that.chatLog(testUserToken,true,"This is question 6");
       that.chatLog(testUserToken,true,"This is question 7");
       that.chatLog(testUserToken,true,"This is question 8");
       that.chatLog(testUserToken,false,"This is answer 8");

       that.chatLog(testUserToken,true,"This is question 9");
       that.chatLog(testUserToken,false,"This is answer 9");
  }

  var readFile = function(fileName){
    var full_path = logRootPath+fileName;
    fs.readFile(full_path, function(err, data) {
    if(err) throw err;
    var array = data.toString().split("\n");
    for(i in array) {
        console.log("["+getTimeStamp()+"] "+array[i]);
    }
   });
  }
  return that;
}


module.exports = new logger();