var timeExtractionEngine = require('./timeExtractionEngine.js');
var timeConvertEngine = require('./timeConvertEngine.js');

var cTimeModule = function() {
	var that = {};

	that.extractTime = function(a) {
        var extracAry = [];

        var timeAry = timeExtractionEngine.time_extraction(a);
        if(timeAry && timeAry.length>0){
        	for (var i = timeAry.length - 1; i >= 0; i--) {
        		extracAry.push(timeAry[i]);
        	};
        }

		return extracAry;
	}

	that.convertTime = function(a) {

        var am_pm_to_hours = timeConvertEngine.am_pm_to_hours(a);
        if(am_pm_to_hours != -1){
        	return am_pm_to_hours;
        }

		return "";
	}

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cTimeModule();