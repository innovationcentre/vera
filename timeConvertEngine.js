var cTimeConvertModule = function(){
  var that = {};
  var UserException = function (err_msg) {
    this.error = err_msg;
  };

  that.am_pm_to_hours = function (timeStr){
      var hours = Number(timeStr.match(/^(\d+)/)[1]);
      var AMPM = timeStr.match(/am|pm/gi);
      if((AMPM !== undefined)&&(AMPM !== null)){
          if ((AMPM == "PM" || AMPM == "pm") && hours < 12) hours = hours + 12;
          if ((AMPM == "AM" || AMPM == "am")&& hours == 12) hours = hours - 12;
          var sHours = hours.toString();
          if (hours < 10) sHours = "0" + sHours;
          if(timeStr.match(/:(\d+)/)){
             var minutes = Number(timeStr.match(/:(\d+)/)[1]);
             var sMinutes = minutes.toString();
             if (minutes < 10) sMinutes = "0" + sMinutes;
             return (sHours  + sMinutes);
          }
          return sHours + '00';
      }else{
          return -1;
         //throw new UserException("Invalid 12 Format");       
      }
  }


  /*input format:yyyymmddTxxxxxx output format: yymmdd*/
  that.alchemyDate_to_standardDate = function (alchemyDate){
      var matchAry = alchemyDate.match(/(\d{4})(\d{2})(\d{2})/);
      if(!matchAry) return -1;

      return matchAry[1].substring(2,4)+matchAry[2]+matchAry[3];
  }

  /*input format: Month Jan/january/01/1 22nd output format: 160122*/
  that.month_date_to_standardDate = function (month,day){
      var myDate = new Date(month + " 1, 2000");
      var monthDigit = myDate.getMonth();
      if(isNaN(monthDigit)) throw new UserException("Invalid month format");
      var currentTime = new Date();
      if(!day.match(/\d+/g)){
      throw new UserException("Invalid day format");
      }
      var myDay = Number(day.match(/\d+/g)[0]);
      return getStandardDate(currentTime.getFullYear(),monthDigit+1,myDay);
  }
  /*get an array of [next monday, next sunday] in standard format:yymmdd*/
  that.getCommingWeek = function (){
     var dateToday = new Date();
     var day = dateToday.getDay();
     var diff = dateToday.getDate() - day + 7;
     var nextSunday = new Date(dateToday.setDate(diff));
     var diff2 = nextSunday.getDate()+6;
     var nextSat = new Date(dateToday.setDate(diff2));
     var snextSunday = getStandardDate(nextSunday.getFullYear(),nextSunday.getMonth()+1,nextSunday.getDate());
     var snextSat = getStandardDate(nextSat.getFullYear(),nextSat.getMonth()+1,nextSat.getDate());
     return new Array(snextSunday,snextSat);
  }

  /*date status of targetDate: two days later of 20160428*/
  /*0 means later,1 means earlier, 2 means same day*/
  that.getDateFromDuration = function (date,status,targetDate){
     var dateNum = myParseInt(date);
     if ( dateNum <= 0) return -1;

     var diff = targetDate.getDate();
     if(status == 0){
       diff =  targetDate.getDate() + dateNum;
     }else if(status == 1){
      diff =  targetDate.getDate() - dateNum;
     }
     var newDate = new Date(targetDate.setDate(diff));
     if (newDate) {
        return getStandardDate(newDate.getFullYear(),newDate.getMonth()+1,newDate.getDate()); 
     }
        return newDate;
  }

  var myParseInt = function (number) {
      var ref = { one:1, two:2, three:3, four:4, five:5, six:6, seven:7, eight:8, nine:9, ten:10, eleven:11, twelve:12, thirteen:13, fourteen:14, fifteen:15, sixteen:16, seventeen:17, eighteen:18, nineteen:19, twenty:20, thirty: 30, forty: 40, fifty: 50, sixty: 60, seventy: 70, eighty: 80, ninety:90, hundred: 100, thousand: 1000, million: 1000000 };

      var find = new RegExp( "(one|t(wo|hree|en|welve|hirteen|wenty|hirty)|f(our|ive|ourteen|iftenn|orty|ifty)|s(ixteen|ixty|eventy|ix|even|eventeen|teen)|eigh(ty|t|teen)|nin(ety|e|eteen)|zero|hundred|thousand|million)", "gi" );
      var mult = new RegExp( "(hundred|thousand|million)", "gi" );
      
      //reversing the string
      number = number.split(' ').reverse().join(" ");
      value = 0; multiplier = 1;
      while( a = find.exec(number) ) {
          if( m = mult.exec(a[0]) ) {
              if( m[0] == 'hundred' ) { multiplier *= 100; }
              else { multiplier = ref[m[0]]; }
          }
          else {
              value += ref[a[0]] * multiplier;
          }
      }
      return value;
  }

  /*input format dd.mm to yymmdd*/
  that.cardinalDate_to_standardDate = function (cardinalStr){
  var currentTime = new Date();
  var myDate = new Date(cardinalStr);
  var tempYear = null;
  var tempMonth = null;
  var tempDate = null;
  if(myDate != "Invalid Date"){
     tempYear = myDate.getFullYear() != "2001"?myDate.getFullYear():currentTime.getFullYear();
     tempMonth = myDate.getMonth()+1;
     tempDate = myDate.getDate();
  }else{
     var pointDateAry = cardinalStr.split(".");
     var slashDateAry = cardinalStr.split("/");
     var spaceDateAry = cardinalStr.split(" ");
     
     var separatorRe = cardinalStr.match(/\.|\/|\s/i);
     if(separatorRe){
        var reDateAry = cardinalStr.split(separatorRe[0]);
        if(reDateAry){
           if(reDateAry.length==2){
             tempYear = currentTime.getFullYear();
             var temD1 = reDateAry[0];
             var temD2 = reDateAry[1];
             if(temD1 >12){
               tempMonth = temD2;
               tempDate = temD1;
             }else{
               tempMonth = temD1;
               tempDate = temD2;
             }
           }else if(reDateAry.length==3){
             tempYear = reDateAry[0];
             tempMonth = reDateAry[2];
             tempDate = reDateAry[3];
           }      
        }
     }
  }
    if(tempYear && tempMonth && tempDate){
      tempDate = Number(tempDate.toString().replace(/st|nd|rd|th/gi, ""));
      return getStandardDate(tempYear,Number(tempMonth),tempDate); 
    }else{
      return -1;
      //throw new UserException("Invalid date format");
    }  
  }

  that.getTodayTomorrowStandardDate = function (dateStr){
   var currentDate = new Date();
   if (dateStr.match(/today/gi)){ 
       return getStandardDate(currentDate.getFullYear(),currentDate.getMonth()+1,currentDate.getDate()); 
   }else if(dateStr.match(/tomorrow/gi)){
       return getStandardDate(currentDate.getFullYear(),currentDate.getMonth()+1,currentDate.getDate()+1); 
   }
   return -1;
  }

    /*convert yyyy mm dd to yymmdd*/
  var getStandardDate = function (year,month,day){
      var sDays = day.toString();
      var sMonths = month.toString();
      if(month<10)sMonths = "0"+sMonths;
      if(month >12) throw new UserException("Invalid month format");
      if(day<10)sDays = "0"+sDays;
      if(day >31) throw new UserException("Invalid day format");
      return (year.toString().substring(2,4)+sMonths+sDays);  
  }
  return that;
}
module.exports = new cTimeConvertModule();
