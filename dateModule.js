var timeExtractionEngine = require('./timeExtractionEngine.js');
var timeConvertEngine = require('./timeConvertEngine.js');

var cLocationModule = function() {
	var that = {};

	that.extractDate = function(a) {
        
        var extracAry = [];        

        var month_date = timeExtractionEngine.month_date_extraction(a);
        if(month_date.length>0){
        	extracAry = extracAry.concat(month_date);
        }
        
        var date_keyword = timeExtractionEngine.date_keyword_extraction(a);
        if(date_keyword != -1){
        	extracAry.push(date_keyword);
        }

        var dur_status_keyword = timeExtractionEngine.extractDurationStatus(a);
        if(dur_status_keyword != -1){
          extracAry = extracAry.concat(dur_status_keyword);
        }

        var today_tomo_extra = timeExtractionEngine.extractTodayTomo(a);
        if(today_tomo_extra != -1){
          extracAry = extracAry.concat(today_tomo_extra);
        }

        var extractCommonDateAry = timeExtractionEngine.extractCommonDate(a);
        if(extractCommonDateAry != -1){
        	extracAry = extracAry.concat(extractCommonDateAry);
        }

        console.log("extractCommonDateAry=" + JSON.stringify(extractCommonDateAry));  
		return extracAry;
	}
/*
*What the Date would be like
* 1:20160419T000000 done
* 2:same day
* 3:April 04(th)
* 4:22.04
* 5: Today/Tomorrow   done
* 6: one day later/earlier done
* 7: coming week done
*/
	that.convertDate = function(a) {

	   if(a == timeExtractionEngine.getDateKeyWord()[0]){
	   	  var resultAry = timeConvertEngine.getCommingWeek();
	      return {"From": resultAry[0],"To": resultAry[1] };
	   }
	   
	   var todayTomorrowStandardDate = timeConvertEngine.getTodayTomorrowStandardDate(a);
	   if(todayTomorrowStandardDate != -1){
	   	 return {"Date":todayTomorrowStandardDate};
	   }

	   var alchemyDateStandardDate = timeConvertEngine.alchemyDate_to_standardDate(a);
     if(alchemyDateStandardDate != -1){
     	return {"Date":alchemyDateStandardDate};
     }

       var isHasTimeStatus = timeExtractionEngine.time_keyword_extraction(a);
       console.log("Asking vera: " + isHasTimeStatus);
       var dateFromDuration = timeConvertEngine.getDateFromDuration(a,isHasTimeStatus,new Date());
       if(dateFromDuration != -1){
       	return {"Date":dateFromDuration};
       }

       var cardinalDateToStandard = timeConvertEngine.cardinalDate_to_standardDate(a);
       if(cardinalDateToStandard != -1){
       		return {"Date":cardinalDateToStandard};
       }


	  return "";
	}

	var init = function() {

	}

	init();

	return that;
}

module.exports = new cLocationModule();